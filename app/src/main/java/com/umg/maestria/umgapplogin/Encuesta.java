package com.umg.maestria.umgapplogin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by marvinmanuelmenchumenchu on 27/07/17.
 */

public class Encuesta {

    private String nombre;
    private String apellido;
    private String sexo;
    private Date fechaNacimiento;
    private List<String> lenguajeProgramacion;

    public Encuesta() {
    }

    public Encuesta(String nombre, String apellido, String sexo, Date fechaNacimiento, List<String> lenguajeProgramacion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.lenguajeProgramacion = lenguajeProgramacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public List<String> getLenguajeProgramacion() {
        return lenguajeProgramacion;
    }

    public void setLenguajeProgramacion(List<String> lenguajeProgramacion) {
        this.lenguajeProgramacion = lenguajeProgramacion;
    }

    @Override
    public String toString() {
        return "Encuesta{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", sexo='" + sexo + '\'' +
                ", fechaNacimiento=" + new SimpleDateFormat("dd/MM/yyyy").format(fechaNacimiento) +
                ", lenguajeProgramacion=" + lenguajeProgramacion +
                '}';
    }
}
