package com.umg.maestria.umgapplogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class DatosActivity extends AppCompatActivity {

    private ListView lv1;
    private List<String> listaEncuesta = new ArrayList<>();;
    private ArrayAdapter<String> adapterEncuesta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);

        lv1= (ListView) findViewById(R.id.lv1 );

        String datos = getIntent().getStringExtra("datos");
        listaEncuesta.add(datos);

        adapterEncuesta=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listaEncuesta);
        lv1.setAdapter(adapterEncuesta);
    }

    public void atras(){
        finish();
        Intent a = new Intent(this, StudentActivity.class);
        startActivity(a);
    }
}
