package com.umg.maestria.umgapplogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StudentActivity extends AppCompatActivity {

    private EditText nom;
    private EditText ape;
    private RadioButton sexM;
    private RadioButton sexF;
    private CalendarView fecN;
    private CheckBox jav;
    private CheckBox php;
    private CheckBox rub;




    private String sexoSeleccionado;
    private List<String> lenguajesSeleccionados = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);



        /*Encuesta encuesta = new Encuesta();
        encuesta.setNombre("Marvin");
        encuesta.setApellido("Menchu");
        encuesta.setSexo("M");
        encuesta.setFechaNacimiento(new Date());
        List<String> listLan = new ArrayList<>();
        listLan.add("Java");
        listLan.add("Php");
        listLan.add("Rubi");
        encuesta.setLenguajeProgramacion(listLan);


        Encuesta encuesta2 = new Encuesta();
        encuesta2.setNombre("Wilkier");
        encuesta2.setApellido("Rosales");
        encuesta2.setSexo("M");
        encuesta2.setFechaNacimiento(new Date());
        List<String> listLan2 = new ArrayList<>();
        listLan.add("Java");
        listLan.add("Php");
        encuesta2.setLenguajeProgramacion(listLan2);



        listaEncuesta.add(encuesta.toString());
        listaEncuesta.add(encuesta2.toString());*/

        nom = (EditText) findViewById(R.id.nombre);
        ape = (EditText) findViewById(R.id.apellido);
        sexM = (RadioButton) findViewById(R.id.masculino);
        sexF = (RadioButton) findViewById(R.id.femenino);
        fecN = (CalendarView) findViewById(R.id.fechaNac);
        jav = (CheckBox) findViewById(R.id.java);
        php = (CheckBox) findViewById(R.id.php);
        rub = (CheckBox) findViewById(R.id.ruby);

        //Utility.setListViewHeightBasedOnChildren(lv1);


    }

    public void add(View view){
        String vNom = nom.getText().toString();
        String vApe = ape.getText().toString();
        Date vFecNac = new Date(fecN.getDate());
        Encuesta enc = new Encuesta();
        enc.setNombre(vNom);
        enc.setApellido(vApe);
        enc.setSexo(sexoSeleccionado);
        enc.setFechaNacimiento(vFecNac);
        enc.setLenguajeProgramacion(lenguajesSeleccionados);

        //listaEncuesta.add(enc.toString());

        //adapterEncuesta.notifyDataSetChanged();

        nom.setText("");
        ape.setText("");
        sexM.setChecked(false);
        sexF.setChecked(false);
        fecN.setClickable(false);
        jav.setChecked(false);
        php.setChecked(false);
        rub.setChecked(false);
        nom.requestFocus();

        Intent d = new Intent(this, DatosActivity.class);
        d.putExtra("datos", enc.toString());
        startActivity(d);

    }

    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.masculino:
                if(checked)
                    sexoSeleccionado = "M";
                break;
            case R.id.femenino:
                if(checked)
                    sexoSeleccionado = "F";
                break;
        }
    }

    public void onCheckboxClicked(View view){
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()){
            case R.id.java:
                if (checked)
                    lenguajesSeleccionados.add("Java");
                else
                    lenguajesSeleccionados.remove("Java");
                break;
            case R.id.php:
                if (checked)
                    lenguajesSeleccionados.add("Php");
                else
                    lenguajesSeleccionados.remove("Php");
                break;
            case R.id.ruby:
                if (checked)
                    lenguajesSeleccionados.add("Ruby");
                else
                    lenguajesSeleccionados.remove("Ruby");
                break;
        }
    }



}
